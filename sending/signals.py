from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now

from .models import Sending, Message, Client


@receiver(post_save, sender=Sending)
def sending_post_save(sender, instance, created, **kwargs):
    print('SIGNAL')
    print(f'{sender=}')
    print(f'{created=}')
    print(f'{instance=}')
    print(f'{kwargs=}')
    if instance.client_filter_mocode:
        clients = Client.objects.all().filter(
            teg=instance.client_filter_teg,
            mocode=instance.client_filter_mocode
        )
    else:
        clients = Client.objects.all().filter(teg=instance.client_filter_teg)

    for client in clients:
        msg = Message(
            uuid_que='',
            create_dt=now(),
            status='new',
            sending=instance,
            client=client,
            message=instance.msg_text.replace('{NAME}', client.name),
            phone=client.phone,
            date_start=instance.start_dt,
            date_end=instance.finish_dt,
            time_zone=client.time_zone
        )
        msg.save()

        # uuid_que = models.CharField(max_length=36, default='', blank=True, verbose_name='uuid сообщения')
        # create_dt = models.DateTimeField(default=now, blank=False, verbose_name='Дата создания')
        # status = models.CharField(max_length=10, default='Новый', blank=False, verbose_name='Статус')
        # sending = models.ForeignKey(Sending, on_delete=models.CASCADE, related_name='sending')
        # client
        # message = models.CharField(max_length=250, default='', blank=True, verbose_name='Статус')
