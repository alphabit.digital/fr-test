from datetime import timedelta, datetime

from frtest.celery import app
import json
import requests
import logging
from requests.exceptions import HTTPError
from django.conf import settings

from .models import Message
from .tools import get_datetime_with_utc

logger = logging.getLogger('celeryLog')
loggerDebug = logging.getLogger('main')


@app.task
def my_task(a, b):
    r = a * b
    return r


@app.task(default_retry_delay=settings.DEFAULT_RETRY_DELAY)
def provider_post(msg_id):
    logger.info(f'Message {msg_id=} try send to provider')

    msg = Message.objects.get(pk=msg_id)
    msg.status = 'Sending'
    msg.save()

    url = settings.PROVIDER_HOST + 'send/' + str(msg.id)
    # Можно JSON decode/encode
    data = "{" + f'"id": {msg.id}, "phones": {msg.phone},"text": "{msg.message}"' + "}"
    headers = {
        'Accept': 'application/json',
        'Authorization': settings.PROVIDER_AUTHORIZATION,
        'Content-Type': 'application/json; charset=UTF-8'
    }

    dtime_end = get_datetime_with_utc(msg.date_end, msg.time_zone)

    if datetime.utcnow() <= dtime_end:
        try:
            response = requests.post(
                url,
                data=data.encode('utf-8'),
                headers=headers
            )
        except HTTPError as http_err:
            res_text = f'HTTP error occurred: {http_err}'
            status_code = response.status_code
        except Exception as other_err:
            res_text = f'Other error occurred: {other_err}'
            status_code = 0
        else:
            status_code = response.status_code
            res_text = 'Success!' if status_code == 200 else response.reason
    else:
        status_code = 0
        res_text = 'Time out'

    if status_code == 200:
        logger.info(f'Message {msg_id=} was send successfully')
    elif status_code == 0:
        logger.error(f'Message {msg_id=} was NOT send with err="{res_text}"')
    else:
        logger.warning(f'Message {msg_id=} was NOT send with err="{res_text}"')

    loggerDebug.info(f"{msg_id=} {status_code=} {res_text=}")

    if status_code != 200:
        if datetime.utcnow() + timedelta(seconds=60) <= dtime_end:
            logger.info(f'Message {msg_id=} will send retry in 60 sec')
            raise provider_post.retry(exc=f'{status_code} {res_text}', countdown=60)
        else:
            msg.status = 'Time over'
    else:
        msg.status = 'Ok'

    msg.save()

    return msg.status


@app.task
def send_message_to_provider(id_msg):
    msg = Message.objects.get(pk=id_msg)

    # Берем разницу между началом, c учетом часового пояса, и сейчас по UTC
    dtime_now = datetime.utcnow()
    dr = get_datetime_with_utc(msg.date_start, msg.time_zone)-dtime_now
    startTimeInSeconds = dr.seconds + dr.days * 24*60*60

    dr = get_datetime_with_utc(msg.date_end, msg.time_zone) - dtime_now
    endTimeInSeconds = dr.seconds + dr.days * 24*60*60

    loggerDebug.info(f'{msg.id=}; {dtime_now=}; {msg.date_start=}; {msg.date_end=}; {startTimeInSeconds=}; {endTimeInSeconds=} ')

    startTimeInSeconds = 0 if startTimeInSeconds < 0 else startTimeInSeconds
    # Если разница отрицательная, значит время старта прошло, проверяем на дату/время конца рассылки,
    if endTimeInSeconds > 0:
        r = provider_post.apply_async((id_msg,), countdown=startTimeInSeconds)
        if startTimeInSeconds > 0:
            msg.status = 'wait'
        loggerDebug.info(f'{msg.id=} send_message_to_provider: uuid= {r.id} {startTimeInSeconds=}; {msg.date_start=}; {msg.time_zone=}; {dtime_now=}')
        msg.uuid_que = r.id
    else:
        msg.uuid_que = -1
        msg.status = 'Time over'
        loggerDebug.info(f'{msg.id=} Time out')

    msg.save()


@app.task
def check_new_message():
    new_messages = Message.objects.all().filter(status='new')
    for msg in new_messages:
        res = send_message_to_provider.delay(msg.id)
        msg.status = 'Sending'
        msg.save()


# celery -A frtest worker --pool=solo --loglevel=INFO
# celery -A frtest beat

