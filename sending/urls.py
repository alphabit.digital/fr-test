from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'sending'

router = DefaultRouter()

router.register(r'v1/client', views.ClientViewSet)
router.register(r'v1/sending', views.SendingViewSet)
router.register(r'v1/message', views.MessagesViewSet)
# router.register(r'v1/messages', views.MessagesAPIView.as_view())

urlpatterns = router.urls

urlpatterns += [

    path('v1/messages/reset', views.ResetMessagesStatus),
    path('v1/messages', views.MessagesAPIView.as_view(), name='api_messages'),

]
