from django.db import models
from django.core.validators import RegexValidator

from django.utils.timezone import now


REGEX_PHONE_NUM = '^7\d{10,10}$'
REGEX_MOCODE = '^\d{1,6}$'
REGEX_TZ = '^(0|\+?\-?([1-9]|1[0-2]))$'


# Create your models here.
# sending - Сущность "рассылка"
# • уникальный id рассылки
# • дата и время запуска рассылки
# • текст сообщения для доставки клиенту
# • фильтр свойств клиентов, на которых должна быть произведена рассылка (код мобильного оператора, тег)
# • дата и время окончания рассылки: если по каким-то причинам не успели разослать все сообщения - никакие сообщения клиентам после этого времени доставляться не должны

class Sending(models.Model):
    title = models.CharField(max_length=120, verbose_name='Название рассылки')
    start_dt = models.DateTimeField(default=now, blank=False, verbose_name='Время начала рассылки')
    finish_dt = models.DateTimeField(default=None, blank=True, verbose_name='Время окончания рассылки')
    msg_text = models.CharField(max_length=250, blank=False, verbose_name='Текст сообщения')
    client_filter_mocode = models.CharField(max_length=10, verbose_name='Фильтр клиентов: Код мобильного оператора',
                                            blank=True, default='')
    client_filter_teg = models.CharField(max_length=30, verbose_name='Фильтр клиентов: тэг', blank=True, default='')
    status = models.CharField(max_length=30, default='Новый', verbose_name='Статус рассылки')
    progress = models.DecimalField(max_digits=4, decimal_places=2, default=0, verbose_name='Прогресс отправки')

    def __str__(self):
        return f'({self.id}) {self.title}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


# client - Сущность "клиент"
# •уникальный id клиента
# •номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
# •код мобильного оператора
# •тег (произвольная метка)
# •часовой пояс

class Client(models.Model):
    name = models.CharField(max_length=50, default='', blank=True, verbose_name='Обращение к клиенту (Имя)')
    phone = models.CharField(max_length=11, verbose_name='Номер телефона', default='7',
                             validators=[RegexValidator(REGEX_PHONE_NUM, 'invalid phone number')]
                             )
    mocode = models.CharField(max_length=10, verbose_name='Код оператора', default='7',
                              validators=[
                                  RegexValidator(REGEX_MOCODE, 'invalid code of mobile operator')
                              ]
                              )
    teg = models.CharField(max_length=30, verbose_name='Тэг')
    time_zone = models.CharField(max_length=3, default='0', verbose_name='Часовой пояс',
                                 validators=[
                                     RegexValidator(REGEX_TZ, 'invalid time zone')
                                 ]
                                 )

    def __str__(self):
        return f'({self.id} {self.name})'

    def save(self, *args, **kwargs):
        if self.phone[0] == '+':
            self.phone = self.phone[1:]
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


# message - Сущность "сообщение" имеет атрибуты:
# • уникальный id сообщения
# • дата и время создания (отправки)
# • статус отправки
# • id рассылки, в рамках которой было отправлено сообщение
# • id клиента, которому отправили

class Message(models.Model):
    # uuid_que = models.UUIDField(default=uuid.uuid4, editable=False, unique=False, blank='True', verbose_name='uuid сообщения')
    uuid_que = models.CharField(max_length=36, default='', blank=True, verbose_name='uuid сообщения')
    create_dt = models.DateTimeField(default=now, blank=False, verbose_name='Дата создания')
    status = models.CharField(max_length=30, default='new', blank=False, verbose_name='Статус')
    sending = models.ForeignKey(Sending, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='clients')
    message = models.CharField(max_length=250, default='', blank=True, verbose_name='Статус')
    phone = models.CharField(max_length=11, verbose_name='Номер телефона', default='7')
    date_start = models.DateTimeField(default=None, blank=False, verbose_name='Начало отправки', null=True)
    date_end = models.DateTimeField(default=None, blank=False, verbose_name='Конец отправки', null=True)
    time_zone = models.CharField(max_length=3, default='0', verbose_name='Часовой пояс клиента')

    def __str__(self):
        return f'[{self.id}] to=[{self.client.id}]{self.client.name}; sending=[{self.sending_id}]{self.sending.title}'
