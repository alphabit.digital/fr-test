from datetime import timedelta


def get_datetime_with_utc(dt, utc=0):
    dt = dt - timedelta(hours=int(utc))
    dt = dt.replace(tzinfo=None)
    return dt
