from django.contrib import admin
from .models import Client, Sending, Message

# Register your models here.

admin.site.register(Client)
admin.site.register(Sending)
admin.site.register(Message)
