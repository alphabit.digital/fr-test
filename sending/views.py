# from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.response import Response
from rest_framework import viewsets, permissions
from rest_framework.views import APIView

from .serializers import ClientSerializer, SendingSerializer, MessageSerializer
from .models import Client, Sending, Message


class ClientViewSet(viewsets.ModelViewSet):
    """ Клиенты """
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    # print('before call task')
    # make_sum_ab.delay(1, 2)


class SendingViewSet(viewsets.ModelViewSet):
    """ Рассылки """
    serializer_class = SendingSerializer
    queryset = Sending.objects.all()
    permission_classes = [permissions.IsAuthenticated]


class MessagesViewSet(viewsets.ModelViewSet):
    """ Сообщения для отправки """
    serializer_class = MessageSerializer
    queryset = Message.objects.all().order_by('id')
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id', 'sending_id', 'client_id']


class MessagesAPIView(APIView):
    """ Сообщения """
    def get(self, request):
        messages = Message.objects.all()
        serializer = MessageSerializer(messages, many=True)

        return Response(serializer.data)


def ResetMessagesStatus(request):
    print('def ResetMessagesStatus(): - running!!!')
    messages = Message.objects.all()
    messages.update(status='new')
    return HttpResponseRedirect(reverse('sending:api_messages'))

