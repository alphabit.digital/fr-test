from rest_framework import serializers
from .models import Client, Sending, Message


class MessageSerializer(serializers.ModelSerializer):
    """Сообщения"""

    class Meta:
        model = Message
        fields = ('__all__')


class ClientSerializer(serializers.ModelSerializer):
    """Работа с моделью 'Клиенты'"""

    class Meta:
        model = Client
        fields = ('__all__')


class SendingSerializer(serializers.ModelSerializer):
    """Рассылки"""
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = Sending
        fields = ('__all__')
