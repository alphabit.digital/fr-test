FROM python

WORKDIR /usr/src/frtest

# переменные окружения для python
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

## Устанавливаем зависимости для Postgre
#RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

# устанавливаем зависимости
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt
RUN python manage.py makemigrations
RUN python manage.py migrate

# создать пользователя

EXPOSE 8000


# копируем содержимое текущей папки в контейнер
COPY . .
